package org.bcd.password.validator;

import org.junit.Assert;
import org.junit.Test;

public class LengthValidatorTest {

    Validator validator = new LengthValidator();

    @Test
    public void testValidate() {
        String pass1 = "huivo6";
        String pass2 = "ASDFGHJK123";
        String pass3 = "fdshajklA89";
        String pass4 = "jfdiso987ad";
        String fail1 = "1234";
        String fail2 = "asdfghjklfdsa76s8";
        String fail3 = "Jvoicjxhufjkl24";

        Assert.assertTrue(validator.validate(pass1).isValid());
        Assert.assertTrue(validator.validate(pass2).isValid());
        Assert.assertTrue(validator.validate(pass3).isValid());
        Assert.assertTrue(validator.validate(pass4).isValid());
        Assert.assertTrue(validator.validate(fail1).isNotValid());
        Assert.assertTrue(validator.validate(fail2).isNotValid());
        Assert.assertTrue(validator.validate(fail3).isNotValid());

    }
}
