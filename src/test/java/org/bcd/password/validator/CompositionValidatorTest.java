package org.bcd.password.validator;

import org.bcd.password.validator.CompositionValidator;
import org.junit.Assert;
import org.junit.Test;

public class CompositionValidatorTest {

    @Test
    public void testValidate() {
        String pass1 = "asdf54";
        String pass2 = "asdf2345";
        String pass3 = "1234asdf";
        String noNumbers = "gdfdfzxcv";
        String noLetters = "1234543";
        String containsCaps = "aXCVdf1234ZXC";
        String startsWithCaps = "GFfkds1234";
        String startsWithSpecialChar = "%asdf1234%$";
        String containsSpecialChar = "asdfghjk1234%^";

        CompositionValidator validator = new CompositionValidator();

        Assert.assertTrue(validator.validate(pass1).isValid());
        Assert.assertTrue(validator.validate(pass2).isValid());
        Assert.assertTrue(validator.validate(pass3).isValid());
        Assert.assertTrue(validator.validate(noNumbers).isNotValid());
        Assert.assertTrue(validator.validate(noLetters).isNotValid());
        Assert.assertTrue(validator.validate(containsCaps).isNotValid());
        Assert.assertTrue(validator.validate(startsWithCaps).isNotValid());
        Assert.assertTrue(validator.validate(startsWithSpecialChar).isNotValid());
        Assert.assertTrue(validator.validate(containsSpecialChar).isNotValid());

    }
}
