package org.bcd.password.validator;

import org.junit.Assert;
import org.junit.Test;

public class ContainsNoUppercaseValidatorTest {

    Validator validator = new ContainsNoUppercaseValidator();

    @Test
    public void testValidate() {
        String pass1 = "asdfghjkl";
        String pass2 = "123467d";
        String pass3 = "567482395";
        String fail1 = "ASDFGHJK123";
        String fail2 = "fdshajklA89";
        String fail3 = "Jvoicjx";

        Assert.assertTrue(validator.validate(pass1).isValid());
        Assert.assertTrue(validator.validate(pass2).isValid());
        Assert.assertTrue(validator.validate(pass3).isValid());
        Assert.assertTrue(validator.validate(fail1).isNotValid());
        Assert.assertTrue(validator.validate(fail2).isNotValid());
        Assert.assertTrue(validator.validate(fail3).isNotValid());

    }
}
