package org.bcd.password.validator;

import org.junit.Assert;
import org.junit.Test;

public class ContainsLowerCaseValidatorTest {

    Validator validator = new ContainsLowerCaseValidator();

    @Test
    public void testValidate() {
        String pass1 = "asdfghjkl";
        String pass2 = "123467d";
        String fail1 = "ASDFGHJK123";
        String fail2 = "567482395";

        Assert.assertTrue(validator.validate(pass1).isValid());
        Assert.assertTrue(validator.validate(pass2).isValid());
        Assert.assertTrue(validator.validate(fail1).isNotValid());
        Assert.assertTrue(validator.validate(fail2).isNotValid());

    }
}
