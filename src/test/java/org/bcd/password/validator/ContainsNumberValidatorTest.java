package org.bcd.password.validator;

import org.junit.Assert;
import org.junit.Test;

public class ContainsNumberValidatorTest {

    Validator validator = new ContainsNumberValidator();

    @Test
    public void testValidate() {
        String pass1 = "123467d";
        String pass2 = "567482395";
        String pass3 = "ASDFGHJK123";
        String pass4 = "fdshajklA89";
        String pass5 = "jfdiso987ad";
        String fail1 = "asdfghjkl";
        String fail2 = "Jvoicjx";

        Assert.assertTrue(validator.validate(pass1).isValid());
        Assert.assertTrue(validator.validate(pass2).isValid());
        Assert.assertTrue(validator.validate(pass3).isValid());
        Assert.assertTrue(validator.validate(pass4).isValid());
        Assert.assertTrue(validator.validate(pass5).isValid());
        Assert.assertTrue(validator.validate(fail1).isNotValid());
        Assert.assertTrue(validator.validate(fail2).isNotValid());
    }
}
