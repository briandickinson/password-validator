package org.bcd.password.validator;

import org.bcd.password.validator.NoConsecutiveSequenceValidator;
import org.junit.Assert;
import org.junit.Test;

public class NoConsecutiveSequenceValidatorTest {

    @Test
    public void testSequence() {
        String fail1 = "abcababcab";
        String fail2 = "abcababcabt";
        String fail3 = "aa";
        String fail4 = "abcbcab";
        String fail5 = "abcbca";
        String fail6 = "abcdefefhbcdhs";
        String fail7 = "abcababca";
        String pass1 = "asdfjiovpja";
        String pass2 = "aba";
        String pass3 = "na";
        String pass5 = "abcb";

        NoConsecutiveSequenceValidator validator = new NoConsecutiveSequenceValidator();
        Assert.assertTrue(validator.validate(fail1).isNotValid());
        Assert.assertTrue(validator.validate(fail2).isNotValid());
        Assert.assertTrue(validator.validate(fail3).isNotValid());
        Assert.assertTrue(validator.validate(fail4).isNotValid());
        Assert.assertTrue(validator.validate(fail5).isNotValid());
        Assert.assertTrue(validator.validate(fail6).isNotValid());
        Assert.assertTrue(validator.validate(fail7).isNotValid());
        Assert.assertTrue(validator.validate(pass1).isValid());
        Assert.assertTrue(validator.validate(pass2).isValid());
        Assert.assertTrue(validator.validate(pass3).isValid());
        Assert.assertTrue(validator.validate(pass5).isValid());

    }
}
