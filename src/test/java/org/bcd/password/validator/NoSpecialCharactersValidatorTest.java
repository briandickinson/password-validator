package org.bcd.password.validator;

import org.junit.Assert;
import org.junit.Test;

public class NoSpecialCharactersValidatorTest {

    Validator validator = new NoSpecialCharactersValidator();

    @Test
    public void testValidate() {
        String pass1 = "asdf54";
        String pass2 = "asdf2345";
        String pass3 = "1234asdf";
        String pass4 = "aXCVdf1234ZXC";
        String pass5_accent = "ùhfduiosafh7";
        String startsWithSpecial = "%asdf1234%$";
        String containsSpecial = "asdfghjk1234%^";
        String containsSpecial2 = "hjfkdla!/";

        Assert.assertTrue(validator.validate(pass1).isValid());
        Assert.assertTrue(validator.validate(pass2).isValid());
        Assert.assertTrue(validator.validate(pass3).isValid());
        Assert.assertTrue(validator.validate(pass4).isValid());
        Assert.assertTrue(validator.validate(pass5_accent).isValid());
        Assert.assertTrue(validator.validate(startsWithSpecial).isNotValid());
        Assert.assertTrue(validator.validate(containsSpecial).isNotValid());
        Assert.assertTrue(validator.validate(containsSpecial2).isNotValid());
    }
}
