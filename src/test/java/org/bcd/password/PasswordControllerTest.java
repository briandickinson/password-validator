package org.bcd.password;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class PasswordControllerTest {

    @Autowired
    private WebApplicationContext context;

    MockMvc mockMvc;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void testPostValidation_valid() throws Exception {
        String testPW = "lakjshdf876";
        mockMvc.perform(post("/passwordValidations")
//                .with(csrf())
                .secure(true)
                .content(testPW)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.valid", is(true)))
                .andExpect(jsonPath("$.validationFailures", is(empty())));
    }

    @Test
    public void testPostValidation_invalid() throws Exception {
        String testPW = "lA%kjshdf876";
        mockMvc.perform(post("/passwordValidations")
//                .with(csrf())
                .secure(true)
                .content(testPW)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.valid", is(false)))
                .andExpect(jsonPath("$.validationFailures", hasSize(2)));
    }

    @Test
    public void testGetValidation_badUuid() throws Exception {

        String fakeUuid = "fhdusa-0f9d8hsanfhds7uaofhjhalsdhflghjlgfh34jl2ghf4g";

        mockMvc.perform(put("/passwordValidations/password")
//          .with(csrf())
            .secure(true)
            .content(fakeUuid)
            .contentType(MediaType.TEXT_PLAIN_VALUE)
            .accept(MediaType.TEXT_PLAIN_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Invalid Id"))
            .andReturn();
    }

    @Test
    public void testValidateAndPostPassword_valid() throws Exception {
        String testPW = "lakjshdf876";
        ObjectMapper mapper = new ObjectMapper();

        String uuid;

        MvcResult postResult = mockMvc.perform(post("/passwordValidations")
//                .with(csrf())
                .secure(true)
                .content(testPW)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.valid", is(true)))
                .andExpect(jsonPath("$.validationFailures", is(empty())))
                .andReturn();

        JSONObject json = new JSONObject(postResult.getResponse().getContentAsString());
        uuid = json.getString("uuid");
        mockMvc.perform(put("/passwordValidations/password")
//                .with(csrf())
                .secure(true)
                .content(uuid)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string("Password saved."));
    }

    @Test
    public void testValidateAndPostPassword_invalid() throws Exception {
        String testPW = "L&akjshdf876";
        ObjectMapper mapper = new ObjectMapper();

        String uuid;

        MvcResult postResult = mockMvc.perform(post("/passwordValidations")
//                .with(csrf())
                .secure(true)
                .content(testPW)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.valid", is(false)))
                .andExpect(jsonPath("$.validationFailures", is(hasSize(2))))
                .andReturn();

        JSONObject json = new JSONObject(postResult.getResponse().getContentAsString());
        uuid = json.getString("uuid");

        mockMvc.perform(put("/passwordValidations/password")
//                .with(csrf())
                .secure(true)
                .content(uuid)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isPreconditionFailed())
                .andExpect(content().string("Password not valid."))
                .andReturn();
    }

    @Test
    public void testValidateAndPostPassword_nonexistent() throws Exception {
        String uuid;

        uuid = UUID.randomUUID().toString();

        mockMvc.perform(put("/passwordValidations/password")
//                .with(csrf())
                .secure(true)
                .content(uuid)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Validation not found for password. Password must first be validated."))
                .andReturn();
    }

    //    @Test
//    public void testGetValidation() throws Exception {
//
//        String testPW = "lakjshdf876";
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.registerModule(new Jackson2HalModule());
//
//        String uuid;
//
//        MvcResult postResult = mockMvc.perform(post("/passwordValidations")
//                .with(csrf())
//                .secure(true)
//                .content(testPW)
//                .contentType(MediaType.TEXT_PLAIN_VALUE)
//                .accept(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.valid", is(true)))
//                .andExpect(jsonPath("$.validationFailures", is(empty())))
//                .andReturn();
//
//        JSONObject json = new JSONObject(postResult.getResponse().getContentAsString());
//        uuid = json.getString("uuid");
//
//        MvcResult getResult = mockMvc.perform(get("/passwordValidations")
//                .with(csrf())
//                .secure(true)
//                .content(uuid)
//                .contentType(MediaType.TEXT_PLAIN_VALUE)
//                .accept(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.valid", is(true)))
//                .andExpect(jsonPath("$.validationFailures", is(empty())))
//                .andReturn();
//
//        JSONObject json2 = new JSONObject(postResult.getResponse().getContentAsString());
//        String uuid2 = json2.getString("uuid");
//
//        Assert.assertEquals(uuid, uuid2);
//    }

}
