package org.bcd.password.service;

import org.bcd.password.model.PasswordValidation;
import org.bcd.password.model.PasswordValidationRepository;
import org.bcd.password.validator.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PasswordValidationServiceImplTest {

    @TestConfiguration
    static class configuration {
        @Bean
        public Validator lengthValidator() {
            return new LengthValidator();
        }

        @Bean
        public Validator noSpecialCharactersValidator() {
            return new NoSpecialCharactersValidator();
        }

        @Bean
        public Validator containsNoUpperCaseValidator() {
            return new ContainsNoUppercaseValidator();
        }

        @Bean
        public Validator containsLowerCaseValidator() {
            return new ContainsLowerCaseValidator();
        }

        @Bean
        public Validator containsNumberValidator() {
            return new ContainsNumberValidator();
        }

        @Bean
        public Validator noConsecutiveSequenceValidator() {
            return new NoConsecutiveSequenceValidator();
        }

        @Bean
        public PasswordValidationRepository passwordValidationRepository() {
            return new PasswordValidationRepository();
        }

        @Bean
        PasswordValidationService passwordValidationService() {
            return new PasswordValidationServiceImpl();
        }
    }

    @Autowired
    PasswordValidationService validationService;

    @Test
    public void testValidatePassword() {
        String pass1 = "asdf54";
        String pass2 = "asdf2345";
        String pass3 = "1234asdf";
        String sequenceNoNumbers = "gdfdfzxcv";
        String noLetters = "1234543";
        String containsCapsTooLong = "aXCVdf1234ZXC";
        String startsWithCaps = "GFfkds1234";
        String startsWithSpecialChar = "%asdf1234%$";
        String containsSpecialCharTooLong = "asdfghjk1234%^";
        String noNumCapsTooLongSpecial = "KD*^jivozjfkdls)(*543";

        PasswordValidation val1 = validationService.validatePassword(pass1);
        Assert.assertTrue(val1.isValid());
        Assert.assertNotNull(val1.getUuid());
        Assert.assertTrue(val1.getValidationFailures().isEmpty());
//        Assert.assertEquals(val1.getPassword(), "*********");

        PasswordValidation val2 = validationService.validatePassword(pass2);
        Assert.assertTrue(val2.isValid());

        PasswordValidation val3 = validationService.validatePassword(pass3);
        Assert.assertTrue(val3.isValid());

        PasswordValidation val4 = validationService.validatePassword(sequenceNoNumbers);
        Assert.assertFalse(val4.isValid());
        Assert.assertTrue(val4.getValidationFailures().size() == 2);

        PasswordValidation val5 = validationService.validatePassword(noLetters);
        Assert.assertFalse(val5.isValid());
        Assert.assertTrue(val5.getValidationFailures().size() == 1);

        PasswordValidation val6 = validationService.validatePassword(containsCapsTooLong);
        Assert.assertFalse(val6.isValid());
        Assert.assertTrue(val6.getValidationFailures().size() == 2);

        PasswordValidation val7 = validationService.validatePassword(startsWithCaps);
        Assert.assertFalse(val7.isValid());
        Assert.assertTrue(val7.getValidationFailures().size() == 1);

        PasswordValidation val8 = validationService.validatePassword(startsWithSpecialChar);
        Assert.assertFalse(val8.isValid());
        Assert.assertTrue(val8.getValidationFailures().size() == 1);

        PasswordValidation val9 = validationService.validatePassword(containsSpecialCharTooLong);
        Assert.assertFalse(val9.isValid());
        Assert.assertTrue(val9.getValidationFailures().size() == 2);

        PasswordValidation val10 = validationService.validatePassword(noNumCapsTooLongSpecial);
        Assert.assertFalse(val10.isValid());
        Assert.assertTrue(val10.getValidationFailures().size() == 3);
//        Assert.assertEquals(val1.getPassword(), "*********");

    }
}
