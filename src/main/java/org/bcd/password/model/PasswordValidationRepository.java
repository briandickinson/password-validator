package org.bcd.password.model;

import org.bcd.password.model.PasswordValidation;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// Pretend like this accesses a database
// This is pretty much a mock class, so I am not including any tests for it
@Repository
public class PasswordValidationRepository {

    ConcurrentMap<String, PasswordValidation> validations = new ConcurrentHashMap<>();

    public void add(PasswordValidation validation) {
        validations.put(validation.getUuid(), validation);
    }

    public PasswordValidation get(String uuid) {
        return validations.get(uuid);
    }
}
