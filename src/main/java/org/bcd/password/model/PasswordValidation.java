package org.bcd.password.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PasswordValidation {

    String uuid;
    boolean valid;
    List<String> validationFailures = new ArrayList<>();

    public PasswordValidation(boolean valid, List<String> failures) {
        this.uuid = UUID.randomUUID().toString();
        this.valid = valid;
        this.validationFailures = failures;
    }

//    public PasswordValidation(String uuid, boolean valid, List<String> failures) {
//        this.uuid = uuid;
//        this.valid = valid;
//        this.validationFailures = failures;
//    }


    public String getUuid() {
        return uuid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValidationFailures(List<String> validationFailures) {
        this.validationFailures = validationFailures;
    }

    public List<String> getValidationFailures() {
        return validationFailures;
    }

    public void addValidationFailure(String failure) {
        this.validationFailures.add(failure);
    }

}
