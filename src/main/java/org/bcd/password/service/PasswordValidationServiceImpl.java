package org.bcd.password.service;

import org.bcd.password.model.PasswordValidationRepository;
import org.bcd.password.model.PasswordValidation;
import org.bcd.password.validator.Validator;
import org.bcd.password.validator.ValidatorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class PasswordValidationServiceImpl implements PasswordValidationService {

    @Autowired
    List<Validator> validators = new ArrayList<>();

    PasswordValidationRepository repository;

    @Autowired
    public void setPasswordValidationRepository(PasswordValidationRepository repository) {
        this.repository = repository;
    }

    public PasswordValidation validatePassword(String password) {
        List<String> failures = new ArrayList<>();

        for (Validator validator : validators) {

            ValidatorResult result = validator.validate(password);
            if (result.isNotValid()) {
                failures.add(result.getMessage());
            }
        }

        PasswordValidation validation = new PasswordValidation(failures.isEmpty(), failures);
        repository.add(validation);
        return validation ;
    }

    public PasswordValidation getPasswordValidation(String uuid) {
        return repository.get(uuid);
    }
}
