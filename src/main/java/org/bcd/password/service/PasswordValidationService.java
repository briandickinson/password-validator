package org.bcd.password.service;

import org.bcd.password.model.PasswordValidation;
import org.springframework.stereotype.Service;

@Service
public interface PasswordValidationService {

    PasswordValidation validatePassword(String password);

    PasswordValidation getPasswordValidation(String uuid);
}
