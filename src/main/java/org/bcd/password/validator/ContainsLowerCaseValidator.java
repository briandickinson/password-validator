package org.bcd.password.validator;

public class ContainsLowerCaseValidator implements Validator {

    static String failureMessage = "Password must contain at least one lowercase letter.";

    @Override
    public ValidatorResult validate(String password) {
        boolean valid = false;

        for (char character : password.toCharArray()) {

            if ( Character.isLowerCase(character)) {

                valid = true;
                break;
            }
        }
        return new ValidatorResult(valid, failureMessage);
    }
}
