package org.bcd.password.validator;

import org.springframework.stereotype.Component;

@Component
public interface Validator {

    /**
     * Returns a ValidatorResult object for the given string
     * @param password
     * @return
     */
    ValidatorResult validate(String password);

}
