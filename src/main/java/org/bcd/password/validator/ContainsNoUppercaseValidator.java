package org.bcd.password.validator;

public class ContainsNoUppercaseValidator implements Validator {

    static String failureMessage = "Password cannot contain uppercase letters.";

    @Override
    public ValidatorResult validate(String password) {
        boolean valid = true;

        for (char character : password.toCharArray()) {

            if ( Character.isUpperCase(character)) {

                valid = false;
                break;
            }
        }
        return new ValidatorResult(valid, failureMessage);
    }

}
