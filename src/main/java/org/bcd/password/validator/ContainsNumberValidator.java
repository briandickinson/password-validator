package org.bcd.password.validator;

public class ContainsNumberValidator implements Validator {

    static String failureMessage = "Password must contain at least one number.";

    @Override
    public ValidatorResult validate(String password) {
        boolean valid = false;

        for (char character : password.toCharArray()) {

            if ( Character.isDigit(character)) {
                valid = true;
                break;
            }
        }
        return new ValidatorResult(valid, failureMessage);
    }
}
