package org.bcd.password.validator;

public class ValidatorResult {

    boolean valid;
    String message;

    public ValidatorResult(boolean valid, String message) {
        this.valid = valid;
        this.message = message;
    }

    /**
     * Returns password validation result
     * @return
     */
    public boolean isValid() {
        return valid;
    }

    public boolean isNotValid() {return !valid;}

    /**
     * Returns a message
     * @return
     */
    public String getMessage() {
        return message;
    }
}
