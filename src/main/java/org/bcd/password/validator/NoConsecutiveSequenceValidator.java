package org.bcd.password.validator;

public class NoConsecutiveSequenceValidator implements Validator {

    static String failureMessage =
            "Password must not contain any sequence of characters immediately followed by the same sequence." +
            "This includes repeating characters such as \"hh\".";

    @Override
    public ValidatorResult validate(String password) {

        boolean isValid = ! detectRepeatingSequenceNoSeparation(password);
        return new ValidatorResult(isValid, failureMessage);
    }

    /**
     * Iterates through all the indices in a String, looking for
     * duplicate character sequences in succession. See the behavior of
     * {@link #findDuplicateSequenceAroundIndex}
     * Returns true upon finding a match or returns false if no match is found.
     * @param str
     * @return
     */
    public boolean detectRepeatingSequenceNoSeparation(String str) {
        boolean foundRepeatingSequence = false;

        for (int i = 1 ; i < str.length() ; i++) {
            if (findDuplicateSequenceAroundIndex(str, i)) {
                foundRepeatingSequence = true;
                break;
            }
        }
        return foundRepeatingSequence;
    }

    /**
     * Looks for duplicate sequences around the index, in forward order
     * splitIndex is exclusive for the first substring, and
     * inclusive for the second substring.
     * E.G For str "abcababc", a splitIndex of 5 will split the
     * string into abcab and abc.  Then it will try to match the
     * beginning of the second substring with the end of the first substring
     * i.e. a == b? , then ab == ab? , etc, returning true if a match is found
     * @param str
     * @param splitIndex
     * @return
     */
    boolean findDuplicateSequenceAroundIndex(String str, int splitIndex) {
        boolean found = false;
        String firstPart = str.substring(0, splitIndex);
        String secondPart = str.substring(splitIndex);

        for (int j = 1 ; j <= firstPart.length(); j++) {
            if (firstPart.regionMatches(firstPart.length() - j, secondPart, 0, j)) {
                found = true;
                break;
            }
        }
        return found;
    }

}
