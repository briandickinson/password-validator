package org.bcd.password.validator;

public class LengthValidator implements Validator {

    static String validationRequirement = "Password must be between 5 and 12 characters in length.";

    @Override
    public ValidatorResult validate(String password) {
        boolean valid = false;
        if (password.length() >= 5 && password.length() <= 12) {
            valid = true;
        }
        return new ValidatorResult(valid, validationRequirement);
    }
}
