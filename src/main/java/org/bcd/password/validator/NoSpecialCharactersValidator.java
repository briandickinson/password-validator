package org.bcd.password.validator;

public class NoSpecialCharactersValidator implements Validator {

    static String failureMessage = "Password may only contain letters or numbers.";

    /**
     * Checks that no special characters are present
     * Allows characters other than
     * @param password
     * @return
     */
    @Override
    public ValidatorResult validate(String password) {
        boolean valid = true;

        for (char character : password.toCharArray()) {

            if ( ! Character.isLetterOrDigit(character)) {
                valid = false;
                break;
            }
        }
        return new ValidatorResult(valid, failureMessage);
    }
}
