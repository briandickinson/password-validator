package org.bcd.password.validator;

import java.util.regex.Pattern;

/**
 * This class is not being used, in favor of individual validators that do not use regular expressions
 * However it does seem to work.
 */
public class CompositionValidator implements Validator {

    static String validationRequirement = "Password must consist of a mixture of lowercase letters " +
            "and numerical digits only, with at least one of each.";

    @Override
    public ValidatorResult validate(String password) {


        Pattern containsLowercase = Pattern.compile("^(?=.*[a-z]).+$");
        Pattern containsDigit = Pattern.compile("^(?=.*\\d).+$");
        Pattern containsNoUppercase = Pattern.compile("^(?!.*[A-Z]).+$");
        Pattern containsOnlyAlphanumeric = Pattern.compile("^([a-z0-9]*$)");

        boolean valid = containsLowercase.matcher(password).matches();
        valid &= containsDigit.matcher(password).matches();
        valid &= containsNoUppercase.matcher(password).matches();
        valid &= containsOnlyAlphanumeric.matcher(password).matches();

        return new ValidatorResult(valid, validationRequirement);
    }
}
