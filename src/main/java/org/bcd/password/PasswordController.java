package org.bcd.password;

import org.bcd.password.model.PasswordValidation;
import org.bcd.password.resource.PasswordValidationResource;
import org.bcd.password.resource.PasswordValidationResourceAssembler;
import org.bcd.password.service.PasswordValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/passwordValidations")
public class PasswordController {

    @Autowired
    PasswordValidationService passwordValidationService;

    @Autowired
    PasswordValidationResourceAssembler assembler;

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public HttpEntity<PasswordValidationResource> postValidation(@RequestBody String password) {

        PasswordValidation validation = passwordValidationService.validatePassword(password);
        PasswordValidationResource resource = assembler.convertToResource(validation);

        return new ResponseEntity(resource, HttpStatus.OK);
    }

    /**
     * Save new password, given a valid PasswordValidation id.
     * This method is unimplemented as far as saving - it just checks to see if there
     * is a valid PasswordValidation in the mocked up PasswordValidationRepository
     * No backend persistence here
     * Slightly unusual mapping - password is a subresource of passwordValidation
     * Basically this represents that the validation must exist before the password can exist
     * @param passwordValidationId
     * @return
     */
    @RequestMapping(
            value = "/password",
            method = RequestMethod.PUT,
            consumes = MediaType.TEXT_PLAIN_VALUE,
            produces = MediaType.TEXT_PLAIN_VALUE
    )
    public HttpEntity postPassword(@RequestBody String passwordValidationId) {

        if ( ! validatePasswordValidationIdInput(passwordValidationId)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid Id");
        }

        HttpEntity response;
        PasswordValidation validation = passwordValidationService.getPasswordValidation(passwordValidationId);
        if (validation == null) {
            response = ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Validation not found for password. Password must first be validated.");
        }
        else if (validation.isValid()) {
            response = ResponseEntity
                    .status(HttpStatus.OK)
                    .body("Password saved.");
        }
        else {
            response = ResponseEntity
                    .status(HttpStatus.PRECONDITION_FAILED)
                    .body("Password not valid.");
        }
        return response;
    }

    boolean validatePasswordValidationIdInput(String id) {
        boolean valid = true;
        try {
            UUID.fromString(id);
        }
        catch (IllegalArgumentException ex) {
            // do something here
            // log it
            valid = false;
        }
        return valid;
    }

    //    @RequestMapping(
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public HttpEntity<PasswordValidationResource> getValidation(@RequestBody String uuid) {
//
//        if ( ! validatePasswordValidationIdInput(uuid)) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
//        }
//
//        PasswordValidation validation = passwordValidationService.getPasswordValidation(uuid);
//        PasswordValidationResource resource = assembler.convertToResource(validation);
//
//        return new ResponseEntity(resource, HttpStatus.OK);
//    }

}
