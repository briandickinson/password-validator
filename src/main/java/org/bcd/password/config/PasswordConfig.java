package org.bcd.password.config;

import org.bcd.password.validator.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PasswordConfig {

//    @Bean
//    public Validator compositionValidator() {
//        System.out.println("Using CompositionValidator");
//        return new CompositionValidator();
//    }

    @Bean
    public Validator lengthValidator() {
        return new LengthValidator();
    }

    @Bean
    public Validator noSpecialCharactersValidator() {
        return new NoSpecialCharactersValidator();
    }

    @Bean
    public Validator containsNoUpperCaseValidator() {
        return new ContainsNoUppercaseValidator();
    }

    @Bean
    public Validator containsLowerCaseValidator() {
        return new ContainsLowerCaseValidator();
    }

    @Bean
    public Validator containsNumberValidator() {
        return new ContainsNumberValidator();
    }

    @Bean
    public Validator noConsecutiveSequenceValidator() {
        return new NoConsecutiveSequenceValidator();
    }
}
