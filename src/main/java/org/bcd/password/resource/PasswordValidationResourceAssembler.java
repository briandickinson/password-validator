package org.bcd.password.resource;

import org.bcd.password.PasswordController;
import org.bcd.password.model.PasswordValidation;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class PasswordValidationResourceAssembler {

    public PasswordValidationResource addDefaultLinks(PasswordValidationResource validation) {

        if (validation.getContent().isValid()) {
            // if valid, post link to save new password via validation id
            validation.add(getLinkForPasswordPost(validation));
        }

//        validation.add(getLinkForPasswordValidationGet(validation));
        return validation;
    }

//    public Link getLinkForPasswordValidationGet(PasswordValidationResource validation) {
//        Link link = linkTo(methodOn(PasswordController.class)
//                        .getValidation(validation.getContent().getUuid())).withSelfRel();
//        return link;
//    }

    public Link getLinkForPasswordPost(PasswordValidationResource validation) {
        Link link = linkTo(methodOn(PasswordController.class)
                .postPassword(validation.getContent().getUuid())).withRel("password");
        return link;
    }

    public PasswordValidationResource convertToResource(PasswordValidation validation) {
        PasswordValidationResource resource = new PasswordValidationResource(validation);
        addDefaultLinks(resource);
        return resource;
    }
}
