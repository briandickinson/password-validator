package org.bcd.password.resource;

import org.bcd.password.model.PasswordValidation;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;


public class PasswordValidationResource extends Resource<PasswordValidation> {

    static String OBFUSCATED_PASSWORD = "*********";

    public PasswordValidationResource(PasswordValidation content, Link... links) {
        super(content, links);
    }


}

