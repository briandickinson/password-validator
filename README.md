A RESTful password validation service is a little bit weird. We are not actually storing the password,
so it is not like normal validation in a REST service. Therefore we don't return an error response
if the validation fails.

The validation can be done on the server side, and in a RESTful manner, by
storing a PasswordValidation object. It could also be possible to use the
PasswordValidation id as a reference that can be passed in a PUT call to a /password
endpoint. In that case the password would only be passed once from the client.

I am going to pretend that we should be storing the PasswordValidation as a resource,
and just use a POST. I am using POST both for security and because the rules can change, therefore
PUT would be incorrect.

I interpreted single letters to be a sequence when in succession, so "lkqrii09" would be invalid.

This project is buildable using Maven:
mvn clean package

To run:
java -jar .\target\passwordValidation-validator-1.0-SNAPSHOT.jar

I used a Firefox add on called RESTClient for testing
The SSL/TLS certificate is not signed, so it will probably be necessary to
bypass the security warning and/or add an exception.
I implemented Spring Security with most of the defaults, however authentication
and CSRF are disabled.

Server port is 8443.  This is configurable in /src/main/resources/application.properties

POST https://localhost:8443/passwordValidations

HTTP Body: fdsaoi8

The http body should contain the password string

Expect a PasswordValidation in the response

{
  "uuid":"05544d69-7758-4d4f-a1f7-cc3211501ef6",
  "valid":false,
  "validationFailures":["Password cannot contain uppercase letters."],
  "_links": {
    "self": {
      "href":  "https://localhost:8443/passwordValidations"
    }
  }
}

Just for fun, I included an extra endpoint:
If the password were valid, an extra link would be included to post the new password,
but using a valid passwordValidation id.

{
  "uuid": "e743b2bd-2817-4a3e-9acd-f410da5b715e",
  "valid": true,
  "validationFailures": [],
  "_links": {
    "password": {
      "href": "https://localhost:8443/passwordValidations/password"
    }
  }
}

PUT https://localhost:8443/passwordValidations/password

Http body should contain the uuid from a PasswordValidation response

i.e.  bec51f4b-041d-4ae8-ac88-78c04b200b81

Expect a response that depends on whether the password was valid or not.

The persistence mechanism for this is unimplemented but the service responds appropriately.
The server looks for an existing PasswordValidation and sends an appropriate response. The
password is not persisted in any of these calls, which would be required for this functionality
to actually work. I am not sure where I would store the password in the meantime - probably
somewhere entirely different. Perhaps I could store it
in Password, and use a PUT to activate it.

See PasswordController.postPassword(@RequestBody String passwordValidationId) {...}


I previously included a GET for /passwordValidations but disabled it
It seems like the passwordValidation id is sensitive enough that it
should not be passed in a GET request without a good reason.